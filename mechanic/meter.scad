// Title: Template for moodmeter for laser cutting
// Author: Ralf Lübben
// Date: 15.7.2018
// License: Creative Commons - Share Alike - Attribution
include <screw_holes.scad>;
r=150;
h=2;
e=.02; // small number

module smily(){
difference() {
    cylinder(r=14, h=2, center=true);
    translate([13,0,0]) cube([2.2,2.2,2.1],center=true); 
    translate([-13,0,0]) cube([2.2,2.2,2.1],center=true);
    translate([0,13,0]) cube([2.2,2.2,2.1],center=true); 
    translate([0,-13,0]) cube([2.2,2.2,2.1],center=true);
    cylinder(r=12, h=2.1, center=true);
}
translate([-5,0,0]) cube([2,18,2.1],center=true);
translate([4,-4,0]) cylinder(r=1.3, h=4, center=true);
translate([4,4,0]) cylinder(r=1.3, h=4, center=true);
}

module smily2(){
difference() {
    cylinder(r=14, h=2, center=true);
    translate([13,0,0]) cube([2.2,2.2,2.1],center=true); 
    translate([-13,0,0]) cube([2.2,2.2,2.1],center=true);
    translate([0,13,0]) cube([2.2,2.2,2.1],center=true); 
    translate([0,-13,0]) cube([2.2,2.2,2.1],center=true);
    cylinder(r=12, h=2.1, center=true);
}
translate([-10,0,0])rotate([0,0,180]){
difference() {
    // This piece will be created:
    cylinder(r=10, h=2, center=true);
    // Everything else listed will be taken away:
    cylinder(r=8, h=2.1, center=true);
    translate([3,0,0]) cube([14,20,3.1],center=true);
}}
translate([4,-4,0]) cylinder(r=1.3, h=4, center=true);
translate([4,4,0]) cylinder(r=1.3, h=4, center=true);
}

module smily1(){
difference() {
    cylinder(r=14, h=2, center=true);
    translate([13,0,0]) cube([2.2,2.2,2.1],center=true); 
    translate([-13,0,0]) cube([2.2,2.2,2.1],center=true);
    translate([0,13,0]) cube([2.2,2.2,2.1],center=true); 
    translate([0,-13,0]) cube([2.2,2.2,2.1],center=true);
    cylinder(r=12, h=2.1, center=true);
}
difference() {
    // This piece will be created:
    cylinder(r=10, h=2, center=true);
    // Everything else listed will be taken away:
    cylinder(r=8, h=2.1, center=true);
    translate([3,0,0]) cube([14,20,3.1],center=true);
}
translate([4,-4,0]) cylinder(r=1.3, h=4, center=true);
translate([4,4,0]) cylinder(r=1.3, h=4, center=true);
}

module plate (){
    difference(){
        cylinder(h,r,r);
        translate([0,-r,-e])cube([2*r,2*r,h+2*e]);
    }
    translate([0,-r,0]) cube([r/3,2*r,h]);
}


module screws_pi(){
    pi_width=65;
    pi_height=30;
    pi_dist_hole=3.5;
    screw_height=10;
    screw_dia=2.75;
    x_off=pi_height/2-3.5;
    y_off=pi_width/2-3.5;
    z_off=-screw_height/2;
    translate([x_off,y_off,z_off])screw_hole(DIN912, M2_5, 20);;
    translate([-x_off,y_off,z_off])screw_hole(DIN912, M2_5, 20);
    translate([x_off,-y_off,z_off])screw_hole(DIN912, M2_5, 20);
    translate([-x_off,-y_off,z_off])screw_hole(DIN912, M2_5, 20);
}

module mc_1811_2(){
    mc_width=11.5;
    mc_height=23;
    mc_screw=27.5;
     screw_height=10;
    screw_dia=2.75;
    cube([mc_height,mc_width,10],center=true);
    x_off=mc_screw/2;
    z_off=-screw_height/2;
    translate([x_off,0,z_off])screw_hole(DIN912, M1_6, 20);
    translate([-x_off,0,z_off])screw_hole(DIN912, M1_6, 20);
}

module simple_pointer(){
    plength=r-45;
    pwidth=15;
    square(size = [plength, pwidth ], center = true);
    translate([plength/2,0,0])circle(d=1.3*pwidth);
    translate([-plength/2,0,0])circle(d=pwidth);
    
}

//module pointer(){
//rotate([0,0,180]){
//{
//    union() {
//      translate([0, 0, 2])
//      difference() {
//        linear_extrude(height = 4) import("body.dxf", convexity=20);
//        #translate([0, 0, -0.1]) linear_extrude(height = 2.2) import("hole1.dxf");
//        #translate([0, 0, -0.1]) linear_extrude(height = 2.2) import("hole2.dxf");
//        #translate([0, 0, -0.1]) linear_extrude(height = 2.2) import("hole3.dxf");
//      }
//      difference() {
//        linear_extrude(height = 4) import("base.dxf");
//        translate([0, 0, -0.1]) linear_extrude(height = 4) import("arm.dxf");
//      }
//    }
//}}}

projection(cut = true){

difference(){
    plate();
    translate([-130,0,0]) rotate([0,0,180])smily();
    translate([-53-17,-110,0]) rotate([0,0,180])smily2();
    translate([-53-17,110,0]) rotate([0,0,180])smily1();
    translate([25,0,0]) screws_pi();
    translate([-25,0,0])mc_1811_2();
}}
//translate([-70,r+20,0])
translate([-70,r+20,0]) rotate([0,0,-90]) {
difference(){
    polygon(points=[[0,0],[10,110],[-10,110]], paths=[[0,1,2]],convexity=5);
    translate([0,97,-10])screw_hole(DIN912, M1_6, 20);
}
}

//simple_pointer();
//translate([-140,0,10])#rotate([0,0,-90]) polygon(points=[[0,0],[10,110],[-10,110]], paths=[[0,1,2]],convexity=5); //simple_pointer();

//translate([-140,0,0]) rotate([0,0,-90]) polygon(points=[[0,0],[10,110],[-10,110]], paths=[[0,1,2]],convexity=5);
// holes rasppi zero
//65 - 3.5 - 3.5
//30 -3.5 - 3.5

// holes servo
//23 x 11.5 holes 27.5


