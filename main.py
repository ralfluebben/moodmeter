'''Copyright (C) 2018 Ralf Lübben

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.'''

from flask import Flask
import variance
import pigpio as pigo

app = Flask(__name__)

mean=0.0
M2=0.0
count=0.0
existingAggregate=(count, mean, M2)
invalid=0.0

pin=18
pic= pigo.pi()
pic.set_PWM_range(pin,100)
pic.set_PWM_frequency(pin,100)
pic.set_PWM_dutycycle(pin,15)


@app.route('/')
def hello_world():
    return 'Hello, use http://<host>/update/<value>'

@app.route('/update/<value>')
def update_vale(value):
    # show the user profile for that user
    global existingAggregate
    global invalid
    global pin
    global pic
    v=float(value)
    max_level=1.0
    min_level=-1.0
    (count, mean, M2) = existingAggregate
    if v >= min_level and v <= max_level:
        existingAggregate = update(existingAggregate, v)
        (mean, variance, sampleVariance) = finalize(existingAggregate)
        pic.set_PWM_dutycycle(pin,level_to_cycle(mean,min_level,max_level))
        return "Mood: Mean "+str(mean)+ " Variance: " +str(sampleVariance) + "\n"
    else:
        invalid=invalid+1
        return "Invalid level, invalid values so far " + str(invalid) + "\n"
        
@app.route('/get')
def get():
    global existingAggregate
    (mean, variance, sampleVariance) = finalize(existingAggregate)
    return "Mood: Mean "+str(mean)+ " Variance: " +str(sampleVariance) + "\n"
        
def level_to_cycle(level,min_level,max_level):
    min_dutycycle=21.0
    max_dutycycle=9.0
    l1 = level + abs(min_level)
    l2 = max_level + abs(min_level) 
    diff_dutycycle=max_dutycycle-min_dutycycle
    scale=l2/diff_dutycycle
    return min_dutycycle+(l1/scale)

def main():
    app.run(host='0.0.0.0')

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        pass
    finally:
        pic.set_PWM_dutycycle(pin,0)
        pic.stop()
