#This article uses material from the Wikipedia article <a href="https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance"</a>, which is released under the <a href="https://creativecommons.org/licenses/by-sa/3.0/">Creative Commons Attribution-Share-Alike License 3.0</a>.

# for a new value newValue, compute the new count, new mean, the new M2.
# mean accumulates the mean of the entire dataset
# M2 aggregates the squared distance from the mean
# count aggregates the number of samples seen so far
def update(existingAggregate, newValue):
    (count, mean, M2) = existingAggregate
    count = count + 1 
    delta = newValue - mean
    mean = mean + delta / count
    delta2 = newValue - mean
    M2 = M2 + delta * delta2

    return (count, mean, M2)

# from https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance
# retrieve the mean, variance and sample variance from an aggregate
def finalize(existingAggregate):
    (count, mean, M2) = existingAggregate
    if count < 2:
        return (mean, float('nan'), float('nan'))
    else:
        (mean, variance, sampleVariance) = (mean, M2/count, M2/(count - 1))
        return (mean, variance, sampleVariance)
