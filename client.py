'''Copyright (C) 2018 Ralf Lübben

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.'''

# Import library for System-specific parameters and functions
import sys
# Import library for extensible library for opening URLs
import urllib2
# Check if number of required arguments are correct
if len(sys.argv) != 4:
  print "Use python " +  sys.argv[0] + " <value> <ip_address> <port>"
else:
  # Read the value from the command line
  value = sys.argv[1]
  # Read the ip address from the command line
  ip_address = sys.argv[2]
  # Read the port number from the command line
  port = sys.argv[3]
  contents = urllib2.urlopen("http://%s:%s/update/%s" % (ip_address, port ,value)).read()
  print contents
