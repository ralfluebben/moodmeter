# Installation on Raspberry Pi Zero W

Install required packages

```
sudo apt-get install git python-flask pigpio python-pigpio python3-pigpio
```

Pigpio enable hardware PWM, which avoids vibration of servo.

```
git clone git@gitlab.com:ralfluebben/moodmeter.git
```

Start the enable pigpio at startup.

```
sudo systemctl start pigpiod
sudo systemctl enable pigpiod
```

Test with
```
wget -qO- localhost:5000/update/5
```
